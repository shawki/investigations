let allBtn = document.getElementsByClassName(`btn`);
for (i = 0; i < allBtn.length; i++) {
    allBtn[i].addEventListener(`click`, function() {
        let allElement = this.nextElementSibling;
        if (allElement.style.display == `block`) {
            allElement.style.display = `none`;
            allElement.style.transition = "all 150s ease-in-out";
        } else {
            allElement.style.display = `block`;
            allElement.style.transition = "all .150s ease-in-out";
        }
    });
}

// this is slide show
let count = 0;
let allPic = [`img/1.jpg`, `img/2.jpg`, `img/3.jpg`, `img/4.jpg`];

function loopPic() {
    document.slide.src = allPic[count];
    if (count < allPic.length - 1) {
        count++;
    } else {
        count = 0;
    }
    setTimeout("loopPic()", 2000);
}
loopPic();
/* End Function Slideshow */
function one() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "1";
}

function two() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "2";
}

function three() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "3";
}

function four() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "4";
}

function five() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "5";
}

function six() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "6";
}

function seven() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "7";
}

function eight() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "8";
}

function nine() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "9";
}

function zero() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "0";
}

function dot() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + ".";
}

function plus() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "+";
}

function div() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "/";
}

function X() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "*";
}

function min() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = x + "-";
}

function C() {
    let x = document.getElementById("res").value;
    document.getElementById("res").value = "";
}

function dos() {
    let x = document.getElementById("res").value;
    let B = eval(x);
    document.getElementById("res").value = B;
}